



class WhileDemo{

	public static void main(String[] args){

		int num = 234;

		int mult = 1;

		int digit;
		

		while(num>0){

			digit = num%10;

			mult *= digit;

			num /= 10;

		}
		System.out.println(mult+" ");
	}
}
