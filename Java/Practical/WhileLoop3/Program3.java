


class WhileDemo{

	public static void main(String[] args){

		int num = 436780521;

		int digit;
		System.out.println("Digits divisible by 2 or 3: ");

		while(num>0){

			digit = num%10;

			if(digit%2 == 0 || digit%3 == 0){

				System.out.print(digit+ " ");
			}
			num /= 10;
		}
	}
}

