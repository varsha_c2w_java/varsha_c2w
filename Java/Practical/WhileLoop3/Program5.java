




class WhileDemo{

	public static void main(String[] args){

		int num = 216985;

		int digit;
		System.out.println("Cubes of even Digits: ");

		while(num>0){

			digit = num%10;

			if(digit%2 == 0){

				System.out.print(digit*digit*digit + " ");
			}
			num /= 10;
		}
	}
}


