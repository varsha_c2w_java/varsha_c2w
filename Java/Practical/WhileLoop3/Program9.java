


class WhileDemo{

	public static void main(String[] args){

		int num = 2469185;

		int sum = 0;

		int digit;

		while(num>0){

			digit = num%10;

			if(digit%2 != 0){

				sum += digit*digit;
			}
			num /= 10;
		}
		System.out.println(sum+ " ");
	}
}


