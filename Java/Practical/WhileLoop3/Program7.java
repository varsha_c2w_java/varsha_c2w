


class WhileDemo{

	public static void main(String[] args){

		int num = 256985;
		int sum = 0;
		int digit;

		while(num>0){

			digit = num%10;

			if(digit%2 == 0){

				sum += digit;
			}
			num /= 10;
		}
		System.out.println("Sum of even digits: "+sum);
	}
}


