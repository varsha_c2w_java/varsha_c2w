


class WhileDemo{
	public static void main(String[] args){

		int num = 256985;
		int product = 1;
		int digit;

		while(num>0){

			digit = num % 10;
			if(digit%2 != 0){

				product = product*digit;
			}
			num /= 10;
		}
		System.out.println("product of odd digits: "+product);
	}
}
