






class WhileDemo{

	public static void main(String[] args){

		int num = 256985;

		int digit;
		System.out.println("Square of odd Digits: ");

		while(num>0){

			digit = num%10;

			if(digit%2 != 0){

				System.out.println(digit*digit+ " ");
			}
			num /= 10;
		}
	}
}


