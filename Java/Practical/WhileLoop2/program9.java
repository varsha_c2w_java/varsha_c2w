



class Demo{

	public static void main(String[] args){

		int num = 214367689;
		int oddCount = 0;
		int evenCount = 0;

		while(num>0){
			int digit = num % 10;

			if(digit % 2 == 0){

				evenCount++;
			}else{
				oddCount++;
			}
			num = num/10;
		}
		System.out.println("odd Count: "+ oddCount);
		System.out.println("even Count: "+ evenCount);
	}
}
		
