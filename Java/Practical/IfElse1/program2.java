

class IfElse2{
	public static void main(String[] args){
		
	        int num1 = 13;	
		if(num1%2 == 1){
			System.out.println(num1+" is an odd number");
		}else if(num1%2 == 0){
			System.out.println(num1+" is an even number");
		}else{
			System.out.println("Invalid output");
		}
	}
}
