



class Odd{
	public static void main(String[] args){

		int num1 = 150;
		int num2 = 101;
		int num3 = 0;

		while(num1>=num2){
			if(num1%2 != 0){
				num3 = num3+num1;
			}
			num1--;
		}
		System.out.println("Sum of odd numbers from 150 to 101: "+ num3);
	}
}
