

class Timeofday{
	public static void main(String[] args){

		int hour = 10;
	        
		if(hour>=5 && hour<12){
			System.out.println("Good morning");
		}else if(hour>=12 && hour<17){
			System.out.println("Good afternoon");
		}else if(hour>17 && hour<=20){
			System.out.println("Good evening");
		}else{
			System.out.println("Good night");
		}
	}
}
