


class StudentInfo{
	public static void main(String[] args){

		float percent = 58.00f;

		if(percent>=75.00){
			System.out.println("first class with distinction");
		}else if(percent<75.00 && percent>=60){
			System.out.println("First class");
		}else if(percent<60 && percent>=45.00){
			System.out.println("Second class");
		}else if(percent<45.00 && percent>=35.00){
			System.out.println("Pass only");
		}else{
			System.out.println("Failed");
		}
	}
}
