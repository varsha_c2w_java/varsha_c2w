




import java.io.*;

class InputDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter your name: ");
		String name = br.readLine();

		System.out.print("Enter Society name: ");
		String socName = br.readLine();
		
		System.out.print("Enter flat No: ");
		int flatNo = Integer.parseInt(br.readLine());

		System.out.print("Enter wing: ");
		char wing = (char)br.read();

		System.out.println("Name: "+name);
		System.out.println("socName: "+socName);
		System.out.println("wing: "+wing);
		System.out.println("flatno: "+flatNo);
	}
}



