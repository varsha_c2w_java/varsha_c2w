


import java.io.*;

class InputDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			System.out.print("Enter company name:  ");
			String compName = br.readLine();

			System.out.print("Enter employee name: ");
			String empName = br.readLine();

			System.out.print("Enter employee Id: ");
			int empId = Integer.parseInt(br.readLine());
			
			System.out.print("Enter employee salary: ");
			double empSal = Double.parseDouble(br.readLine());
			
			System.out.println("company name: "+compName);
			System.out.println("employee name: "+empName);
			System.out.println("employee Id: "+empId);
			System.out.println("employee salary: "+empSal);
		
	}
}




