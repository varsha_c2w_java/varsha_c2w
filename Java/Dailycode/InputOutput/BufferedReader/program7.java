





import java.io.*;

class InputDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter company name : ");
		String compName = br.readLine();

		System.out.println("Enter employee name : ");
		String empName = br.readLine();
		
		System.out.println("Enter employee Id : ");

		int empId = Integer.parseInt(br.readLine());

		System.out.println("Company name: "+compName);
		System.out.println("Employee name: "+empName);
		System.out.println("Employee Id: "+empId);
	}
}

