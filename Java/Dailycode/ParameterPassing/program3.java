


class Xyz{

	void gun(){
		System.out.println("In gun");
	}

	void fun(int x){
		System.out.println("In fun");
		System.out.println(x);
	}
}

class Demo{

	void run(float i, float f){
		System.out.println("In run");
		System.out.println(i);
		System.out.println(f);
	}
	public static void main(String[] args){

		Xyz obj = new Xyz();
		obj.gun();
		obj.fun(10);

		Demo obj2 = new Demo();

		obj2.run(50,20);
	}
}
