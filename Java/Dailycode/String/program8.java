


class StringDemo{
	public static void main(String[] args){

		//way 1
		String str1 = "Shashi";
		System.out.println(str1);

		//way 2
		char arr[] = new char[]{'a','s','h','i','s','h'};
		System.out.println(arr);

		String str2 = new String("Core2web");
		System.out.println(str2);
	
	}
}
