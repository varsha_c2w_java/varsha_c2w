


class StringDemo{
	public static void main(String[] args){

		String str1 = "C2W";  //String literal(SCP)

		System.out.println(System.identityHashCode(str1));

		String str2 = "C2W";
		System.out.println(System.identityHashCode(str2));

		String str3 = new String("C2W");   //new string (heap)
		System.out.println(System.identityHashCode(str3));
		
		String str4 = new String("C2W");
		System.out.println(System.identityHashCode(str4));
	}
}
