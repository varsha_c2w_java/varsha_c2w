

class SwitchDemo{
	public static void main(String[] args){
		String friends = "Kanha";
		System.out.println("before switch");

		switch(friends){
			case "Ashish":
				System.out.println("Barclays");
				break;
			case "Kanha":
				System.out.println("BMC Software");
				break;
			case "Badhe":
				System.out.println("IBM");
				break;
			default:
				System.out.println("In switch state");
				
		}
		System.out.println("after switch");
	}
}
