

class SideTri{
       public static void main(String[] args){

	       int rows =4;
	       int col=0;

	       for(int i=1; i<=rows*2-1; i++){

		       if(i<=rows){
			       col = i;
		       }else{
			       col--;
		       }

		       for(int j=1; j<=col; j++){

			       System.out.print("# ");
		       }
		       System.out.println();
	       }
       }
}
