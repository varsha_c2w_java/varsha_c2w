

class SideTri{
	public static void main(String[] args){

		int rows = 4;
		int col = 0;

		for(int i=1; i<=rows*2-i; i++){
			if(i<=rows){
				col=rows-i;
			}else{
				col = i-rows;
			}

			for(int sp=1; sp<=col; sp++){
				System.out.print("  ");
			}
			if(i<=rows){
				col=i;
			}else{
				col=rows*2-i;
			}

			for(int j=1; j<=col; j++){
				System.out.print("* ");
			}
		        System.out.println();
		
		}
	
	}
}

