


class Pattern{
	public static void main(String[] args){

		int rows = 3;
		char ch = 'a';

		for(int i=1; i<=rows;  i++){
			int num = 4;

			for(int j=1; j<=i; j++){

				if(j%2 == 1){
					System.out.print(num+" ");
					num+=2;
				}else{
					System.out.print(ch+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}
