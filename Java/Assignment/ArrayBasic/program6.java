


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");

		int size = sc.nextInt();

		char ch[] = new char[size];
		System.out.println("Enter element of char array: ");

		for(int i = 0; i<size; i++){
			ch[i] = sc.next().charAt(0);
		}
		System.out.println("The characters are: ");
		for(int i = 0; i<size; i++){
			System.out.println(ch[i]+ " ");
		}
		System.out.println();
	}
}
