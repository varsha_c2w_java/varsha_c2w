


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){

			arr[i] = sc.nextInt();
		
		}

		System.out.println("Odd indexed elements of array are: ");

		for(int i = 1; i<size; i+=2){
			System.out.println(arr[i]+" is an odd indexed element");
		}
		}
	}

