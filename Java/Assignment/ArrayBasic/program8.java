


import java.util.*;
class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size:  ");
		int size = sc.nextInt();

		int arr[] = new int[size];
	
		for(int i = 0; i<size; i++){

			System.out.println("Enter employee age"+(i+1)+": ");
			arr[i] = sc.nextInt();
		}

		System.out.println("Employees age are: ");
		for(int i = 0; i<size; i++){
			System.out.println(arr[i]);
		}
	}
}
