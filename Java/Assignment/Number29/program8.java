



import java.util.*;
class DuckNum{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number: ");
		int num = sc.nextInt();

		int temp = num;

		while(num>0){
			int rem = num%10;
			if(rem == 0){
				System.out.println(temp+" is a Duck number");
				break;
			}else{
				System.out.println(temp+" is not a duck num");
			}
			num /= 10;
		}
	}
}
