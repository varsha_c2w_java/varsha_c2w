

import java.util.Scanner;

class Array{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        System.out.print("Enter the number of columns: ");
        int cols = sc.nextInt();

        int[][] array = new int[rows][cols];
        System.out.println("Enter the elements of the array:");

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        System.out.println("Corner elements of the array:");

        if (rows > 0 && cols > 0) {
            System.out.println("Top-left corner: " + array[0][0]);
            System.out.println("Top-right corner: " + array[0][cols - 1]);
            System.out.println("Bottom-left corner: " + array[rows - 1][0]);
            System.out.println("Bottom-right corner: " + array[rows - 1][cols - 1]);
        } else {
            System.out.println("Array dimensions should be greater than zero.");
        }
    }
}

