import java.util.Scanner;

class Array {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

   
        System.out.print("Enter the number of rows and columns (n x n): ");
        int n = sc.nextInt();

        int[][] array = new int[n][n];
        System.out.println("Enter the elements of the array:");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = sc.nextInt();
            }
        }

        int primaryDia = 0;
        for (int i = 0; i < n; i++) {
            primaryDia += array[i][i];
        }

        int secondaryDia = 0;
        for (int i = 0; i < n; i++) {
            secondaryDia += array[i][n - 1 - i];
        }

        int productOfSums = primaryDia * secondaryDia;

        System.out.println("The product of the sums of the primary and secondary diagonals is: " + productOfSums);
    }
}

