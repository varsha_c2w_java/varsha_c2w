

import java.util.Scanner;

class Array {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        
        System.out.print("Enter the no of rows and cols (n x n): ");
        int n = sc.nextInt();

        int[][] array = new int[n][n];
        System.out.println("Enter the elements of the array:");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = sc.nextInt();
            }
        }

        int product = 1;
        for (int i = 0; i < n; i++) {
            product *= array[i][i];
        }

        System.out.println("The product of the primary diagonal elements is: " + product);
    }
}

