import java.util.Scanner;

class Array{

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of rows and columns (n x n): ");
        int n = sc.nextInt();

        int[][] array = new int[n][n];
        System.out.println("Enter the elements of the array:");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += array[i][n - 1 - i];
        }

        System.out.println("The sum of the secondary diagonal elements is: " + sum);
    }
}

