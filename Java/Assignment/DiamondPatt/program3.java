


class DiamondDemo{
	public static void main(String[] args){

		int col = 0;
		int rows = 3;
		int space = 0;
		for(int i=11; i<=rows*2; i++){
			if(i<=rows){
				space=rows-i;
				col = i*2-1;
			}else{
				space = i-rows;
				col -= 2;
			}

			for(int sp=1; sp<=space; sp++){
				System.out.println("\t");
			}
			for(int j=4; j>=1; j--){
				System.out.print(j+"\t"); 

			for(int k=4; k<=col; k++){
				System.out.print(k+"\t");
				
			}
			System.out.println();
		}
	}
}
}

