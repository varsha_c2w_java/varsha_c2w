


class DiamondDemo{
	public static void main(String[] args){

		int col = 0;
		int rows = 3;

		int space = 0;

		int num = 1;
		for(int i=1; i<rows*2; i++){
			if(i<=rows){
				space = rows-i;
				col = i*2-1;
			}else{
				space = i-rows;
				col -= 2;
			}

			for(int sp =1; sp<=space; sp++){
				System.out.print("\t");
			}

			for(int j=1; j<=col; j++){
				System.out.print(num+"\t");
				num++;
			}
			System.out.println();
		}
	}
}

