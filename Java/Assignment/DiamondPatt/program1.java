


class DiamondDemo{
	public static void main(String[] args){

		int col = 0;
		int row = 3;
		int space = 0;

		for(int i=1; i<row*2; i++){
			if(i<=row){
				space = row-i;
				col = i*2-1;
			}else{
				space = i-row;
				col -= 2;
			}

			for(int sp = 1; sp<=space; sp++){
				System.out.print("\t");
			}

			for(int j=1; j<=col; j++){
				System.out.print("1\t");
			}
			System.out.println();
		}
	}
}
