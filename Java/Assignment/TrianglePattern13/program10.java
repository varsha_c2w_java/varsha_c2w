



import java.util.*;
class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter no of rows: ");
		int rows = sc.nextInt();
		//char ch = 'D';

		for(int i = rows; i>=1; i--){

			for(int j=i; j>=1; j--){
				if(i%2 == 0){
					System.out.print((char)(j+64)+" ");
				}else{
					System.out.print((char)(j+96)+" ");
				}

		        
			}
			System.out.println();
		}
		
	}
}

