




import java.util.*;

class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows:");
		int rows = sc.nextInt();

		//char ch = 'A';

		for(int i=1; i<=rows; i++){

			char ch = 'A';
			for(int j=i; j<=rows; j++){
				if(i%2 == 1){
					System.out.print(ch+" ");
					ch++;
				}else{
					System.out.print((char)(ch+32)+" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}



