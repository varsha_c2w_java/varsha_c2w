


import java.util.*;
class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter no of rows: ");
		int rows = sc.nextInt();
		int num = 19;

		for(int i = 1; i<=rows; i++){

			for(int j=i; j<=rows; j++){
				System.out.print(num+" ");
				num-=2;
			}
			System.out.println();
		}
	}
}

