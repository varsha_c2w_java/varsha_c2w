


import java.util.*;
class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter no of rows: ");
		int rows = sc.nextInt();
		
		for(int i=1; i<=rows; i++){

	         	int num = rows;
			char ch = 'C';
			for(int j=i; j<=rows; j++){
				if(i%2 == 1){
					System.out.print(num+" ");
					num--;
				}else{
					System.out.print(ch+" ");
					ch--;
				}
				

			}
			System.out.println();
		}
	}
}
