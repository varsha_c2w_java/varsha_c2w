




import java.util.*;

class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows:");
		int rows = sc.nextInt();

		char ch = 'J';
		for(int i=1; i<=rows; i++){

			for(int j=i; j<=rows; j++){
				System.out.print(ch+" ");
				ch--;
			}
	
			System.out.println();
		}
	}
}

