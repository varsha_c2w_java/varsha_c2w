



import java.util.*;
class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter no of rows: ");
		int rows = sc.nextInt();
	
		//char ch = 'a';
		for(int i = 1; i<=rows; i++){

			int num = 1;
			char ch = 'a';
		
			for(int j=i; j<=rows; j++){

				if(j%2 == 1 || j==1 || j==3){
					System.out.print(num+" ");
					num++;
		
				}else{
					System.out.print(ch+" ");
					ch++;
				}
			}
	
			System.out.println();
		}
	}
}


