


import java.util.*;

class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Elements divisible by 3 :");

		int sum = 0;

		for(int i = 0; i<size; i++){
			if(arr[i]%3 == 0){
				System.out.println(arr[i]);
				sum += arr[i];
			}
		}
		System.out.println("Sum of elements divisible by 3 is: "+sum);
	}
}

