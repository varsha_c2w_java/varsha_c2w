


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter element you want to search: ");
		int x;
		int flag = 0;
		x = sc.nextInt();

		for(int i = 0; i<size; i++){
			if(arr[i]==x){
				flag = 1;
			}else{
				flag = 0;
			}
			if(flag == 1){
				System.out.println("Element found at index: "+i);
			}
		}
	}
}



