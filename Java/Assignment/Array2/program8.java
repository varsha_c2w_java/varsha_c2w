


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter element of array: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		int sum = 0;
		for(int i = 0; i<size; i++){
			if(arr[i]>5 && arr[i]<9){
				System.out.println(arr[i]+" is greater than 5 but less than 9");
			}
		}
	}
}
