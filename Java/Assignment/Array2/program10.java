


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter element of array: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		
		int var = 0;
		int max = arr[0];
		for(int i = 0; i<size; i++){
			if(arr[i]>max){
				max = arr[i];
				var = i;
			}
		}
		System.out.println("Maximum number in an array is found at pos "+var+" is "+max);
	}
}
			
