import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		char ch[] = new char[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<size; i++){
			ch[i] = sc.next().charAt(0);
		}

		System.out.println("Enter characters are: ");

		for(int i = 0; i<size; i++){

			if(ch[i] == 'a' || ch[i]=='e' || ch[i]=='i' || ch[i]=='o' || ch[i]=='u'){
				System.out.println("vowel "+ch[i]+" found at index"+i);
			}
		}
		System.out.println();
	}
}


