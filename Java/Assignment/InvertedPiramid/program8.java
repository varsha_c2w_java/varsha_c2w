


import java.util.*;
class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows: ");
		int rows = sc.nextInt();

		int num = 1;
		for(int i=1; i<=rows; i++){
			for(int s=1; s<i; s++){
				System.out.println("\t");
			}
			for(int j=1; j<=(rows-i)*2+1; j++){

				if(j<(rows-i+1)){
					System.out.print(num++ +"\t");
				}else{
					System.out.print(num-- +"\t");
				}
			}
			num+=2;
			System.out.println();
		}
	}
}
