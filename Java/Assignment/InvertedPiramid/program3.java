




import java.util.*;
class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number: ");
		int rows = sc.nextInt();

		int num = rows;
	        for(int i=1; i<=rows; i++){
			for(int s = 1; s<i; s++){

				System.out.print("\t");
			}
			for(int j=1; j<=(rows-i)*2+1; j++){
				System.out.print(num+"\t");
			}
			num--;
			System.out.println();
		}
	}
}
