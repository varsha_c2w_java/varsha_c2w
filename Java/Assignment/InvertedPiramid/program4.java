




import java.util.*;
class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number: ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){
			int num=1;

			for(int j=1; j<i; j++){
				System.out.print("\t");
			}

			for(int k=1; k<=(rows-i)*2+1; k++){

				System.out.print(num++ +"\t");
			}
			System.out.println();
		}
	}
}


