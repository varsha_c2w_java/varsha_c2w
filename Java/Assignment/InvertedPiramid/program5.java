


import java.util.*;
class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter rows: ");
		int rows = sc.nextInt();

		char ch = 'A';

		for(int i=1; i<=rows; i++){

			for(int j=1; j<i; j++){
				System.out.print("\t");
			}
			for(int s=1; s<=(rows-i)*2+1; s++){
				System.out.print(ch+"\t");
			}
			ch++;
			System.out.println();
		}
	}
}




