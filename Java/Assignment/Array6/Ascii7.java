



import java.util.*;
class Ascii7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		for(int i=0; i<size; i++){
			if(arr[i]>=65 && arr[i]<=90){
				System.out.print((char)arr[i]+" ");
			}else{
				System.out.print(arr[i]+" ");
			}
		}
	}
}

