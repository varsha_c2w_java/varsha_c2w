import java.util.Scanner;

class Reverse8 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of elements in the char array:");
        int n = sc.nextInt();
        sc.nextLine(); 

        char[] array = new char[n];
        System.out.println("Enter the elements of the char array:");
        for (int i = 0; i < n; i++) {
            array[i] = sc.next().charAt(0);
        }

        System.out.println("Alternate elements of the original array:");
        for (int i = 0; i < n; i += 2) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        reverseArray(array);

        System.out.println("Alternate elements of the reversed array:");
        for (int i = 0; i < n; i += 2) {
            System.out.print(array[i] + " ");
        }
    }

    public static void reverseArray(char[] array) {
        int left = 0, right = array.length - 1;
        while (left < right) {
            char temp = array[left];
            array[left] = array[right];
            array[right] = temp;
            left++;
            right--;
        }
    }
}

