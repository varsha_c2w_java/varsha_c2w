

import java.util.Scanner;

class Palindrome9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of elements in the array:");
        int n = scanner.nextInt();
        scanner.nextLine();  

        String[] array = new String[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextLine();
        }

        int palindromeCount = 0;

   
        for (int i = 0; i < n; i++) {
            if (isPalindrome(array[i])) {
                palindromeCount++;
            }
        }
        System.out.println("Count of palindrome elements in the array: " + palindromeCount);
    }
    static boolean isPalindrome(String str) {
        int left = 0, right = str.length() - 1;
        while (left < right) {
            if (str.charAt(left) != str.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}

