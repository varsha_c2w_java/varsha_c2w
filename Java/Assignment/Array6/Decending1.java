

import java.util.*;
class Decending1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		int flag = 0;

		for(int j = 0; j<size-1; j++){
			if(arr[j]>=arr[j+1]){
				flag++;
			}else{
				flag = 1;
			}
		}
		if(flag>=0){
			System.out.println("The given array is decending order");
		}else{
		        System.out.println("Given array is not in decending order");
		}
	}
}

