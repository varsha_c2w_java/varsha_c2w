


import java.util.Scanner;
class CombineArray5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

      
        System.out.println("Enter the number of elements in the first array:");
        int size1 = sc.nextInt();

        int[] array1 = new int[size1];
        System.out.println("Enter the elements of the first array:");
        for (int i = 0; i < size1; i++) {
            array1[i] = sc.nextInt();
        }

        System.out.println("Enter the number of elements in the second array:");
        int size2 = sc.nextInt();

        int[] array2 = new int[size2];
        System.out.println("Enter the elements of the second array:");
        for (int i = 0; i < size2; i++) {
            array2[i] = sc.nextInt();
        }

        int[] combinedArray = new int[size1 + size2];
        for (int i = 0; i < size1; i++) {
            combinedArray[i] = array1[i];
        }
        for (int i = 0; i < size2; i++) {
            combinedArray[size1 + i] = array2[i];
        }

        System.out.println("Combined array:");
        for (int i = 0; i < combinedArray.length; i++) {
            System.out.print(combinedArray[i] + " ");
        }
    }
}

