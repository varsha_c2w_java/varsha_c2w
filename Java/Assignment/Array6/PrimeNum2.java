import java.util.Scanner;
class PrimeNum2 {

    static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter no of elements in the array:");
        int n = sc.nextInt();

        int[] array = new int[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }

        int sum = 0;
        int count = 0;

        for (int i = 0; i < n; i++) {
            if (isPrime(array[i])) {
                sum += array[i];
                count++;
            }
        }
        System.out.println("Sum of all prime numbers in the array: " + sum);
        System.out.println("Count of prime numbers in the array: " + count);
    }
}

