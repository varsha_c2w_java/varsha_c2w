import java.util.Scanner;
class Count3{

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of elements in the array:");
        int n = sc.nextInt();

        int[] array = new int[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }

        System.out.println("Enter the key to count and potentially replace:");
        int key = sc.nextInt();

        int count = 0;
        for (int i = 0; i < n; i++) {
            if (array[i] == key) {
                count++;
            }
        }

        if (count > 2) {
            int cube = key * key * key;
            for (int i = 0; i < n; i++) {
                if (array[i] == key) {
                    array[i] = cube;
                }
            }
        }

        System.out.println("Modified array:");
        for (int i = 0; i < n; i++) {
            System.out.print(array[i] + " ");
        }
    }
}

