


class MixedPatt{
	public static void main(String[] args){

		int rows = 3;
		//char ch = 'C';

		for(int i = 1; i<=rows; i++){
			char ch = 'C';
			int num = 1;

			for(int j=1; j<=rows; j++){

				if(i%2 == 1){
					System.out.print((char)ch+" ");
					ch--;
				}else{
					System.out.print(num+" ");
					num++;
				}
			}
			System.out.println();
		}
	}
}
