


import java.util.*;

class Square{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();

		int num = rows;

		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){

				if(i%2 == 1 && j%2 == 0){
					System.out.print("$ ");
				}else{
					if(i%2 == 1){
						System.out.print(num++ +" ");
					}else{
						System.out.print(num+" ");
					}
				}
			}
			System.out.println();
		}
	}
}
