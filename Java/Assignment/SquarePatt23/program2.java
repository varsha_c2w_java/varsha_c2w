

import java.util.*;
class Square{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no of rows: ");
		int rows = sc.nextInt();

		int ch = 96+rows;

		for(int i =1; i<=rows; i++){
			for(int j = 1; j<=rows; j++){

				if((i+j)<=rows){

					System.out.print((char)ch+" ");
				}else{
					System.out.print((char)(ch-32)+" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
				
