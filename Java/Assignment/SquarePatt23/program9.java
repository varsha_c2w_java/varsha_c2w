



import java.util.*;
class Square{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){

			int num1  = rows*rows;
			int num2 = (rows*2-2)*rows;
			int diff = i*2;

			for(int j=1; j<=rows; j++){
				if(i==1 || i==rows){

					if(j%2 == 0){
						System.out.print("@\t");
					}else{
						System.out.print(num1+"\t");
						num1-=diff;
					}
				}else{
					if(j%2 == 0){
						System.out.print("@\t");
					}else{
						System.out.print(num2+"\t");
						num2-=diff;
					}
				}
			}
			System.out.println();
		}
	}
}




