


import java.util.Scanner;
class Square {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows:");
        int rows = sc.nextInt();

        for (int i = 1; i <= rows; i++){
		int num = rows*rows;

		for(int j=1; j<=rows; j++){

			if(i%2 == 1){
				System.out.print(num-- +" ");
			}else if(i%2 == 1){
				System.out.print(num+" ");
				num = num%1;
			}else{
				System.out.print(num+" ");
			}
		}
		System.out.println();
	}
    }
}
