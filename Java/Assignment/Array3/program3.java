


import java.util.*;
class Array3{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter array elemnts: ");

		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter specific number: ");
		int num = sc.nextInt();
		int count = 0;

		for(int i=0; i<arr.length; i++){
			if(num == arr[i]){
				count++;
			}
		}
		System.out.println(count+" ");
	}
}
