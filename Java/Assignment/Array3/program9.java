
import java.util.*;
class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		int primeNum = arr[0];
		for(int i = 1; i<arr.length; i++){
			primeNum = arr[i];
			int count = 0;
			for(int j = 2; j<arr[i]; j++){
				if(primeNum%j == 0){
					count++;
					
				}
			}
			if(count == 0){
				System.out.println(primeNum+" ");
			}
		}
		System.out.println();
			
	}
}

