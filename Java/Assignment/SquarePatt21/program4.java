



import java.util.*;

class Square{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows:");
		int rows = sc.nextInt();

		int num = 3;
		char ch = 'C';

		for(int i=1; i<=rows; i++){

			for(int j=1; j<=rows; j++){
				if(j == 1){
					System.out.print(ch+" ");
					ch += 3;
				}else{
					System.out.print(num+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}

