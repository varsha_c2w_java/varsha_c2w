





import java.util.*;

class Square{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows:");
		int rows = sc.nextInt();

		int num = 3;
		for(int i=1; i<=rows; i++){

			for(int j=1; j<=rows; j++){
				if(num%2 == 1){
					System.out.print(num*num + " ");
				}else{
					System.out.print(num+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}

