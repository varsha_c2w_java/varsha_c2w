




import java.util.*;

class Square{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows:");
		int rows = sc.nextInt();
		char ch = 'A';
		int num = 3;

		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(num%2 ==1){
					System.out.print(ch+" ");
				}else{
					System.out.print(num+" ");
				}
				
				num++;
			}
			ch++;
			System.out.println();
		}
	}
}
