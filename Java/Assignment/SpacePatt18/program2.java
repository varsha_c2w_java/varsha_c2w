


import java.util.*;

class Space{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();

		for(int i = 0; i<rows; i++){
			for(int j = i; j<rows-1; j++){
				System.out.print("  ");
			}
			int num = rows - i;
			for(int k=0; k<=i; k++){
				  System.out.println(num);
			          if(k<i){
					  System.out.print(" ");
				  }
				  num--;
			}
			System.out.println();
		}
	}
}
