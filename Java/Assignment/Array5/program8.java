




import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		int min1 = arr[0];
		for(int i = 0; i<size;i++){
			if(min1 >= arr[i]){
				min1 = arr[i];
			}
		}
		int min2 = arr[0];
		for(int i=0; i<size; i++){
			if(min2 >= arr[i] && arr[i]>min1){
				min2 = arr[i];
			}
		}
		System.out.println("The second largest element in the array is: "+min2);
	}
}

