

import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		boolean palindrome = true;
		for(int i = 0; i<size/2; i++){
			if(arr[i] != arr[size-i-1]){
				palindrome = false;
			}
		}
		if(palindrome){
			System.out.println("The array is palindrome");
		}else{
			System.out.println("The array is not a palindrome");
		}
	}
}
