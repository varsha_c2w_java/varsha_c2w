



import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		for(int i =0; i<size; i++){
			int mult = 1;
			while(arr[i]>=1){
				mult *= arr[i];
				arr[i]--;
			}
			System.out.println(mult+" ");
		}
	}
}

