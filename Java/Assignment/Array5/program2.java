


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter element of array: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		int oddsum = 0;
		int evensum = 0;
		for(int i = 0; i<size; i++){

			if(arr[i]%2 == 0){
				evensum += arr[i];
			}else{
				oddsum += arr[i];
			}

		}
		System.out.println("odd Sum: "+oddsum);
		System.out.println("even Sum: "+evensum);
	}
}

