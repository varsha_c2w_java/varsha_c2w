



import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}
		
		for(int i = 0; i<size; i++){
			int count = 0;

			for(int j=2; j<arr[i]; j++){
				if(arr[i]%j == 0){
					count++;
				}
			}
			if(count == 0){
				System.out.println("First prime number found at index"+i);
			}
		}
	}
}


