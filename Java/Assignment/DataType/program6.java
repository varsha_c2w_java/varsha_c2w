


class Demo{

	public static void main(String[] args){

		int date = 10;
		String month = "January";
		int year = 2024;

		long daySec = 24*60*60;
		long monthSec = 30*daySec;
		long yearSec = 365*monthSec;

		System.out.println(date);
		System.out.println(month);
		System.out.println(year);
		System.out.println(daySec);
		System.out.println(monthSec);
		System.out.println(yearSec);
	}
}

