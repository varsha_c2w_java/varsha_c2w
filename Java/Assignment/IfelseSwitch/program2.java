

class SwitchDemo{

	public static void main(String[] args){

		char perc = 'A';

		if(perc>='O'){
			System.out.println("Outstanding");
		}else if(perc<'O' && perc>='A'){
			System.out.println("Excellent");
		}else{
			System.out.println("Invalid");
		}
	}
}		
