


import java.util.*;

class Pattern{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("No of Rows= ");
		int rows = sc.nextInt();

		for(int i=1; i<=rows; i++){
			for(int j=rows; j>=i; j--){
				System.out.print("  ");
			}
			for(int j=1; j<=i+i-1; j++){
				System.out.print(1 +" ");
			}
			System.out.println();
		}
	}
}
