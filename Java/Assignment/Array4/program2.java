


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		int diff = 0;

		int min = arr[0];
		for(int i = 0; i<size; i++){
			if(arr[i]<min){
				min = arr[i];
			}
		}
		int var = 0;
		int max = arr[0];
		for(int  i = 0; i<size; i++){
			if(arr[i]>max){
				max = arr[i];
				var = i;
			}
		}
		diff = max - min;
		System.out.println("The diff bet max and min element is: "+diff);
	}
}
