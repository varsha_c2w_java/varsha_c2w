


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");

		int size = sc.nextInt();
		char ch[] = new char[size];

		System.out.println("Enter array characters: ");

		for(int i = 0; i<size; i++){
			ch[i] = sc.next().charAt(0);
		}

		System.out.println("Enter character key: ");
		char key = sc.next().charAt(0);

		int ch1 = ch[0];
		int count = 0;
		for(int i=0; i<size; i++){
			if(ch1 == key){
				count ++;
				break;
			}
			System.out.println(ch1+" ");

		}
	}
}
