

import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();
		char ch[] = new char[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<size; i++){
			ch[i] = sc.next().charAt(0);
		}

		System.out.println("Enter characters are: ");

		for(int i = 0; i<size; i++){
			if('a'<=ch[i] && 'z'>=ch[i]){
				System.out.println(ch[i]+ " ");
			}else{
				System.out.println("#");
			}
		}
	}
}


