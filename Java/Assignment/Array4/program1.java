


import java.util.*;

class Array{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements: ");

		for(int i = 0; i<size; i++){
			arr[i] = sc.nextInt();
		}

		int sum = 0;
		int avg = arr[0];

		for(int i = 0; i<size; i++){

			avg = arr[i];

			sum += arr[i];

			avg = sum/size;
		}
		System.out.println("Array elements avg is: "+avg);
	}
}
			

